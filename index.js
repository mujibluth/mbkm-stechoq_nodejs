// import module http to init server
const https = require('http');

// import module url to init server
const url = require('url');

// create server
const server = http.createServer((req, res) => {
    console.log(url.parse(req.url));
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World\n');
});

// start server
server.listen(
    3000, 
    console.log('Server running at http://localhost:3000/')
);